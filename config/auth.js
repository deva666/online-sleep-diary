var buildConfig = require('./buildConfig')

module.exports = {
	'facebookAuth': {
		'clientID': '',
		'clientSecret': '',
		'callbackURL': 'http://localhost:' + buildConfig.port + '/auth/facebook/callback'
	},
	'googleAuth': {
		'clientID': '',
		'clientSecret': '',
		'callbackURL': 'http://localhost:' + buildConfig.port + '/auth/google/callback'
	}
};
