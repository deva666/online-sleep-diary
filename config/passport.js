var FacebookStrategy = require('passport-facebook')
	.Strategy;
var GoogleStrategy = require('passport-google-oauth')
	.OAuth2Strategy;

var User = require('../app/models/user');

var configAuth = require('./auth');

module.exports = function (passport) {
	passport.serializeUser(function (user, done) {
		done(null, user.id);
	});

	passport.deserializeUser(function (id, done) {
		User.findById(id, function (err, user) {
			done(err, user);
		});
	});

	// =========================================================================
	// FACEBOOK ================================================================
	// =========================================================================
	passport.use(new FacebookStrategy({

		clientID: configAuth.facebookAuth.clientID,
		clientSecret: configAuth.facebookAuth.clientSecret,
		callbackURL: configAuth.facebookAuth.callbackURL,
		passReqToCallback: true, // allows us to pass in the req from our route (lets us check if a user is logged in or not)
		profileFields: ['email', 'displayName', 'name']
	},
		function (req, token, refreshToken, profile, done) {
			process.nextTick(function () {
				if (!req.user) {

					User.findOne({
						'facebook.id': profile.id
					}, function (err, user) {
						if (err)
							return done(err);

						if (user) {
							if (!user.facebook.token) {
								user.facebook.token = token;
								user.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
								user.facebook.email = (profile.emails[0].value || '')
									.toLowerCase();
								user.email = user.facebook.email;
								user.name = user.facebook.name;

								user.save(function (err) {
									if (err)
										return done(err);

									return done(null, user);
								});
							}

							return done(null, user);
						} else {
							var newUser = new User();
							newUser.facebook.id = profile.id;
							newUser.facebook.token = token;
							newUser.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
							newUser.facebook.email = (profile.emails[0].value || '')
								.toLowerCase();
							newUser.email = newUser.facebook.email;
							newUser.name = newUser.facebook.name;

							newUser.save(function (err) {
								if (err)
									return done(err);

								return done(null, newUser);
							});
						}
					});

				} else {
					// user already exists and is logged in, we have to link accounts
					var user = req.user;

					user.facebook.id = profile.id;
					user.facebook.token = token;
					user.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
					user.facebook.email = (profile.emails[0].value || '')
						.toLowerCase();
					user.email = user.facebook.email;
					user.name = user.facebook.name;

					user.save(function (err) {
						if (err)
							return done(err);

						return done(null, user);
					});

				}
			});

		}));


	// =========================================================================
	// GOOGLE ==================================================================
	// =========================================================================
	passport.use(new GoogleStrategy({

		clientID: configAuth.googleAuth.clientID,
		clientSecret: configAuth.googleAuth.clientSecret,
		callbackURL: configAuth.googleAuth.callbackURL,
		passReqToCallback: true

	},
		function (req, token, refreshToken, profile, done) {

			process.nextTick(function () {
				if (!req.user) {

					User.findOne({
						'google.id': profile.id
					}, function (err, user) {
						if (err)
							return done(err);

						if (user) {
							if (!user.google.token) {
								user.google.token = token;
								user.google.name = profile.displayName;
								user.name = profile.displayName;
								user.google.email = (profile.emails[0].value || '')
									.toLowerCase();
								user.email = user.google.email;
								user.name = user.google.name;

								user.save(function (err) {
									if (err)
										return done(err);

									return done(null, user);
								});
							}

							return done(null, user);
						} else {
							var newUser = new User();

							newUser.google.id = profile.id;
							newUser.google.token = token;
							newUser.google.name = profile.displayName;
							newUser.name = profile.displayName;
							newUser.google.email = (profile.emails[0].value || '')
								.toLowerCase(); // pull the first email
							newUser.email = newUser.google.email;
							newUser.name = newUser.google.name;

							newUser.save(function (err) {
								if (err)
									return done(err);

								return done(null, newUser);
							});
						}
					});

				} else {
					var user = req.user;
					user.google.id = profile.id;
					user.google.token = token;
					user.google.name = profile.displayName;
					user.google.email = (profile.emails[0].value || '')
						.toLowerCase(); // pull the first email
					user.email = user.google.email;
					user.name = user.google.name;

					user.save(function (err) {
						if (err)
							return done(err);

						return done(null, user);
					});

				}

			});
		}));
};
