module.exports = {
    isDebugging: process.env.NODE_ENV === 'debug',
    port: 5000
}