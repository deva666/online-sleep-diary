var express = require('express');
var app = express();
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var path = require('path');
var buildConfig = require('./config/buildConfig')
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var database = require('./config/database.js');

mongoose.connect(database.url);

require('./config/passport')(passport);

var appName = 'Sleep Diary';

app.appName = appName;
app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

app.set('view engine', 'jade');
app.set('view options', {
	layout: false
});
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
	secret: '5b369dd8eefbc60ac2880e9538448121'
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

var port = process.env.PORT || buildConfig.port;
require('./app/routes.js')(app, passport);
app.listen(port);