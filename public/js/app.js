var app = angular.module('app', ['ngAnimate', 'diaryServices', 'diaryDirectives']);

app.controller('summaryCtrl', ['$scope', '$rootScope', 'dataService', 'dateService', 'user',
	function ($scope, $rootScope, dataService, dateService, user) {
		var templates = [
			{
				'name': 'cards',
				'url': 'partials/cards.jade'
			}, {
				'name': 'chart',
				'url': 'partials/chart.jade'
			}, {
				'name': 'grid',
				'url': 'partials/grid.jade'
			}, {
				'name': 'noData',
				'url': 'partials/noData.jade'
			}];

		var breakIntoChunks = function (srcArray, chunk) {
			if (chunk === 0) {
				return srcArray;
			}
			var chunks = [];
			for (var i = 0; i < srcArray.length; i += chunk) {
				chunks.push(srcArray.slice(i, i + chunk));
			}
			return chunks;
		}

		var loadData = function () {
			$scope.loading = true;
			dataService.getData($scope.currentDate, $scope.dateRange)
				.success(onGetDataSuccess)
				.error(onGetDataError);
		}

		var onGetDataSuccess = function (data) {
			$scope.overallStats = data.overallStats;
			$scope.days = data.dateRange.days;
			$scope.stats = data.dateRange.stats;
			$scope.sleepGoal = data.sleepGoal;
			$scope.today = data.today;
			$scope.isDataEmpty = data.dateRange.isDataEmpty;
			$scope.hasEntries = data.hasEntries;
			loadTemplate();

			var dayCount = data.dateRange.days.length;

			if (dayCount > 0) {
				$scope.rowChunks = breakIntoChunks(data.dateRange.days, 4);
				$scope.beginDate = data.dateRange.days[0].date;
				$scope.endDate = data.dateRange.days[dayCount - 1].date;
				$scope.momentEndDate = moment($scope.endDate);
			}

			$scope.loading = false;
		};

		var onGetDataError = function (data, status, headers) {
			$scope.loading = false;
			$scope.errorMessage = data.error;
		}

		$scope.user = user;
		$scope.hasEntries = user.hasEntries;
		$scope.fromDate = Date.now();
		$scope.toDate = null;
		$scope.currentDate = new Date();
		$scope.isExpanded = false;
		$scope.dateRange = $scope.user ? $scope.user.selectedDateRange || 'isoWeek' : 'isoWeek';
		$scope.loadData = loadData;

		$scope.setTemplate = function (name) {
			if ($scope.selectedTemplate && $scope.selectedTemplate.name === name) {
				return;
			}

			$scope.loading = true;
			templates.forEach(function (t) {
				if (t.name == name) {
					$scope.selectedTemplate = t;
					$scope.user.selectedTemplate = t.name;
					dataService.updateSettings($scope.user);
				}
			});
		}

		var loadTemplate = function () {

			if (!$scope.hasEntries) {
				$scope.selectedTemplate = templates.find(function (t) {
					return t.name === 'noData'
				});
			} else if (!$scope.user.selectedTemplate && $scope.hasEntries) {
				$scope.setTemplate('cards');
			} else if ($scope.selectedTemplate && $scope.selectedTemplate.name === $scope.user.selectedTemplate) {
				return;
			} else {
				$scope.selectedTemplate = templates.find(function (t) {
					return t.name === $scope.user.selectedTemplate;
				}) || templates[0];
			}
		}

		loadTemplate();
		loadData();

		$rootScope.$on('reloadTemplate', loadTemplate);
		$rootScope.$on('reloadData', loadData);

		$scope.templateLoaded = function () {
			$scope.loading = false;
		}


		$scope.nextDateRange = function () {
			if ($scope.dateRange === 'isoWeek') {
				$scope.currentDate.addDays(7);
			} else if ($scope.dateRange === 'month') {
				$scope.currentDate.addMonths(1);
			}
			$scope.isExpanded = false;
			loadData();
		}

		$scope.previousDateRange = function () {
			if ($scope.dateRange === 'isoWeek') {
				$scope.currentDate.addDays(-7);
			} else if ($scope.dateRange === 'month') {
				$scope.currentDate.addMonths(-1);
			}
			$scope.isExpanded = false;
			loadData();
		}

		$scope.toggleDateRange = function () {
			$scope.dateRange = $scope.dateRange === 'isoWeek' ? 'month' : 'isoWeek';
			$scope.user.selectedDateRange = $scope.dateRange;
			dataService.updateSettings($scope.user);
			loadData();
		}

		$scope.toggleDay = function (day) {
			day.show = !day.show;
		}

		$scope.toggleExpanded = function () {
			$scope.rowChunks.forEach(function (r) {
				r.forEach(function (d) {
					d.show = !$scope.isExpanded;
				});
			});
			$scope.isExpanded = !$scope.isExpanded;
		}

		$scope.deleteEntry = function (entry) {
			dataService.deleteData(entry._id)
				.success(loadData)
				.error(function (err) {
					$scope.userMsg = err;
				});
		}

		$scope.dismissUserMsg = function () {
			$scope.userMsg = null;
		}

		$scope.toggleOptions = function (entry) {
			if (!entry.showOptions) {
				entry['showOptions'] = true;
			} else {
				entry.showOptions = false;
			}
		}

		$scope.formatTime = function (date) {
			return user.localization.timeFormat === 'AM/PM'
				? moment(date).format('hh:mm A') : moment(date).format('HH:mm');
		}

		$scope.formatDate = function (date) {
			return moment(date).format($scope.user.localization.dateFormat);
		}
	}]);

app.controller('userCtrl', ['$scope', '$rootScope', '$window', 'dataService', 'timeService', 'user',
	function ($scope, $rootScope, $window, dataService, timeService, user) {
		var generateSleepGoals = function () {
			var goals = [];
			var hour = 3.5;
			for (var i = 0; i < 13; i++) {
				hour += 0.5;
				goals.push({
					textValue: timeService.formatHours(hour),
					hour: hour
				});
			};
			return goals;
		};

		$scope.user = user;
		$scope.errorMessage = null;
		$scope.message = null;
		$scope.loading = false;
		$scope.sleepGoals = generateSleepGoals();
		$scope.timeFormats = ['24h', 'AM/PM'];
		$scope.dateFormats = [{ example: 'DD.MM.YYYY - 20.03.1999', format: 'DD.MM.YYYY' },
			{ example: 'DD/MM/YYYY - 20/03/1999', format: 'DD/MM/YYYY' },
			{ example: 'MM.DD.YYYY - 03.20.1999', format: 'MM.DD.YYYY' },
			{ example: 'MM/DD/YYYY - 03/20/1999', format: 'MM/DD/YYYY' },
			{ example: 'MMM Do YYYY - Mar 20th 1999', format: 'MMM Do YYYY' },
			{ example: 'MMMM Do YYYY - March 20th 1999', format: 'MMMM Do YYYY' }];

		$scope.saveSettings = function (onSaveEvent) {
			$scope.loading = true;
			$scope.errorMessage = null;
			$scope.message = null;
			dataService.updateSettings($scope.user)
				.success(function (data) {
					$scope.loading = false;
					if (typeof onSaveEvent !== 'undefined') {
						$rootScope.$emit(onSaveEvent);
					} else {
						$window.location.reload();
					}
				})
				.error(function (data, status, he) {
					$scope.loading = false;
					$scope.errorMessage = data.error;
				});
		};

	}]);

app.controller('entryCtrl', ['$scope', '$rootScope', '$timeout', 'dataService', 'timeService', 'user',
	function ($scope, $rootScope, $timeout, dataService, timeService, user) {
		$scope.fromDate = moment()
			.startOf('day')
			.add(-2, 'hours');
		$scope.toDate = moment()
			.startOf('day')
			.add(6, 'hours');
		$scope.user = user;
		$scope.sleepTime = $scope.toDate.to($scope.fromDate, true);
		$scope.postSuccess = false;
		$scope.loading = true;
		$scope.fromTime = new Time($scope.fromDate.hours(), $scope.fromDate.minutes());
		$scope.toTime = new Time($scope.toDate.hours(), $scope.toDate.minutes());

		dataService.getAwakeFeelings()
			.success(function (data) {
				$scope.loading = false;
				$scope.awakeFeelings = data;
				var maxRate = Math.max.apply(null, $scope.awakeFeelings.map(function (f) {
					return f.rate;
				}));
				$scope.awakeFeeling = maxRate;
			})
			.error(function (data, status, he) {
				$scope.loading = false;
				$scope.errorMessage = data.error;
			});

		$scope.insertEntry = function () {
			$scope.loading = true;
			$scope.errorMessage = null;
			var entry = {
				fromDate: $scope.fromDate.toDate(),
				toDate: $scope.toDate.toDate(),
				comments: $scope.comments,
				awakeFeeling: $scope.awakeFeeling
			}
			dataService.postData(JSON.stringify(entry))
				.success(function () {
					$scope.postSuccess = true;
					$scope.loading = false;
					$scope.comments = '';
					$scope.errorMessage = null;
					var maxRate = Math.max.apply(null, $scope.awakeFeelings.map(function (f) {
						return f.rate;
					}));
					$scope.awakeFeeling = maxRate;
					$rootScope.$emit('reloadData');
					$timeout(function () {
						$scope.postSuccess = false;
					}, 2000);
				})
				.error(function (data, status, headers) {
					$scope.loading = false;
					$scope.errorMessage = data.error;
					$scope.postSuccess = false;
				});
		}

		$scope.resetData = function () {
			$scope.postSuccess = false;
			$scope.errorMessage = null;
			$scope.comments = '';
			$scope.awakeFeeling = $scope.awakeFeelings[0];
		};
	}]);