#!/bin/bash
uglifyjs --compress --output app.min.js  -- app.js 
uglifyjs --compress --output site.min.js  -- site.js
uglifyjs --compress --output services.min.js  -- services.js
uglifyjs --compress --output directives.min.js  -- directives.js