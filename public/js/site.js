$(document)
	.ready(function () {
		$(document)
			.on('click', 'a, button', function (e) {
				e.target.blur();
			});
	});

Date.isLeapYear = function (year) {
	return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
};

Date.getDaysInMonth = function (year, month) {
	return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.isLeapYear = function () {
	return Date.isLeapYear(this.getFullYear());
};

Date.prototype.getDaysInMonth = function () {
	return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};

Date.prototype.addMonths = function (value) {
	var n = this.getDate();
	this.setDate(1);
	this.setMonth(this.getMonth() + value);
	this.setDate(Math.min(n, this.getDaysInMonth()));
	return this;
};

Date.prototype.addDays = function (days) {
	this.setDate(this.getDate() + days);
	return this;
};

var Time = function (hours, minutes, amPm) {
	this.hours = hours;
	this.minutes = minutes;
	this.amPm = amPm;
	this.compare = function (time) {
		var thisTotalMinutes = this.hours * 60 + this.minutes;
		var otherTotalMinutes = time.hours * 60 + time.minutes;
		if (thisTotalMinutes === otherTotalMinutes)
			return 0;
		else if (thisTotalMinutes < otherTotalMinutes)
			return -1;
		else
			return 1;
	};
	this.toString = function () {
		var am = this.amPm === 'AM';
		var mins = this.minutes < 10 ? '0' + this.minutes : String(this.minutes);
		if (typeof this.amPm === 'undefined' || am) {
			var hrs = this.hours < 10 ? '0' + this.hours : String(this.hours);
			return hrs + ':' + mins;
		} else {
			var hrs = this.hours + '12';
			return hrs + ':' + mins;
		}
	}
};