angular.module('diaryServices', [])
	.factory('dataService', function ($http) {
		return {
			getData: function (date, dateRange) {
				return $http.get('/user/data', {
					params: {
						date: date,
						dateRange: dateRange
					}
				});
			},
			postData: function (data) {
				return $http.post('/user/data', data);
			},
			deleteData: function (id) {
				console.log(id);
				return $http.delete('/user/data/' + id);
			},
			getAwakeFeelings: function () {
				return $http.get('/feelings');
			},
			updateSettings: function (settings) {
				return $http.post('/user/settings', settings);
			},
			getSettings: function () {
				return $http.get('/user/settings');
			}
		};
	})
	.factory('timeService', function () {
		return {
			getTimerValues: function (fromDate, toDate) {
				var fromMinutes = fromDate.hour() * 60 + fromDate.minute();
				var toMinutes = fromMinutes + toDate.diff(fromDate, 'minutes');
				return [fromMinutes, toMinutes];
			},
			stringToTime: function (input) {
				var splitted = input.split(':');
				return {
					hours: Number(splitted[0]),
					minutes: Number(splitted[1])
				}
			},
			timeToString: function (time) {
				var hours = time.hours < 10 ? '0' + time.hours : String(time.hours);
				return hours + ':' + time.minutes;
			},
			formatHours: function (input) {
				var hours = typeof input === 'String' ? input : String(input);
				var splitted = hours.split('.');
				var formatted = '';
				if (splitted[0].length === 1) {
					formatted += '0' + splitted[0];
				} else {
					formatted += splitted[0];
				}
				if (typeof splitted[1] === 'undefined') {
					formatted += ':00';
				} else {
					var minutesPart = '0.' + splitted[1];
					var minutes = Math.round((Number(minutesPart) * 60));
					formatted += ':' + (minutes < 10 ? '0' + minutes : minutes);
				}
				return formatted;
			}
		};
	})
	.factory('dateService', function () {
		return {
			addDays: function (date, days) {
				var result = new Date(date);
				result.setDate(result.getDate() + days);
				return result;
			}
		};
	});