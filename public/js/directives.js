angular.module('diaryDirectives', ['diaryServices'])
	.directive('chartHolder', function () {
		function getDate(d) {
			return new Date(d.date);
		}

		function getSleepTime(d) {
			return d.sleepHours || 0;
		}

		function getComments(d) {
			return d.entries.map(function (e) {
				return e.comments
			})
				.join('<br/>');
		}

		function translate(x, y) {
			return 'translate(' + x + ',' + y + ')';
		}

		function link(scope, element, attrs, ngModelCtrl) {
			var data, sleepGoal, width, height;

			var minHeight = 300;

			var getSize = function () {
				width = $(window)
					.width() / 1.15;
				var desiredHeight = $(window)
					.height() / 1.50;
				height = desiredHeight > minHeight ? desiredHeight : minHeight;
			}

			function initChart() {
				getSize();
				data = scope.data;
				sleepGoal = scope.sleepGoal;
				if (data) {

					d3.selectAll('svg')
						.remove();

					var margin = {
						top: 10,
						right: 90,
						bottom: 30,
						left: 50
					};
					var leftPadding = 20;
					width = width - margin.left - margin.right - leftPadding;
					height = height;

					var x = d3.time.scale()
						.range([0, width])
						.domain(d3.extent(data, getDate));
					var y = d3.scale.linear()
						.range([height, 0])
						.domain([0, (d3.max([sleepGoal, d3.max(data, getSleepTime)]) + 1)]);
					var xAxis = d3.svg.axis()
						.scale(x)
						.orient('bottom')
						.ticks(data.length > 7 ? data.length / 2 : data.length);
					var yAxis = d3.svg.axis()
						.scale(y)
						.orient('left')
						.ticks(4);

					var svg = d3.select('.chart')
						.append('svg')
						.attr('width', width + margin.left + margin.right + leftPadding)
						.attr('height', height + margin.top + margin.bottom)
						.append('g')
						.attr('transform', translate(margin.left, margin.top));

					svg.append('g')
						.attr('class', 'x axis')
						.attr('transform', translate(leftPadding, height))
						.call(xAxis);

					svg.append('g')
						.attr('class', 'y axis')
						.call(yAxis)
						.append('text')
						.attr('y', '10')
						.attr('x', '45')
						.attr('dy', '.71em')
						.style('text-anchor', 'end')
						.text('hours');

					var line = d3.svg.line()
						.interpolate('monotone')
						.x(function (d) {
							return x(getDate(d))
						})
						.y(function (d) {
							return y(getSleepTime(d))
						});

					var sleepGoalLine = d3.svg.line()
						.x(function (d) {
							return x(getDate(d))
						})
						.y(function (d) {
							return y(sleepGoal)
						});

					var tooltip = d3.select('.chart')
						.append('div')
						.attr('class', 'grid-tooltip');

					svg.append('path')
						.datum(data)
						.attr('class', 'line')
						.attr('d', line)
						.attr('transform', translate(leftPadding, 0));

					svg.append('path')
						.datum(data)
						.attr('class', 'thin-line')
						.attr('d', sleepGoalLine)
						.attr('transform', translate(leftPadding, 0));

					svg.append('g')
						.selectAll('.circle')
						.data(data)
						.enter()
						.append('circle')
						.attr('cx', function (d) {
							return x(getDate(d))
						})
						.attr('cy', function (d) {
							return y(getSleepTime(d))
						})
						.attr('r', function (d) {
							return d.sleepTime ? 9 : 0
						})
						.attr('class', 'point')
						.attr('transform', translate(leftPadding, 0))
						.style('fill', function (d) {
							switch (d.awakeFeeling) {
								case 1:
									return '#FF3834';
								case 2:
									return 'blue';
								case 3:
									return '#2BBB2D';
							}
						})
						.on('mouseover', function (d) {
							tooltip.html(getComments(d));
							tooltip.classed('visible', true);
							tooltip.style("top", (d3.event.pageY - 10) + "px");
							tooltip.style("left", (d3.event.pageX + 10) + "px");
						})
						.on('mouseout', function () {
							tooltip.text('');
							tooltip.classed('visible', false);
						});

					svg.append('g')
						.selectAll('text')
						.data(data)
						.enter()
						.append('text')
						.attr('x', function (d) {
							return x(getDate(d))
						})
						.attr('y', function (d) {
							return y(getSleepTime(d))
						})
						.attr('dy', function (d, i) {
							return i % 2 === 0 ? -10 : -25;
						})
						.attr('transform', translate(leftPadding, 0))
						.style('text-anchor', 'middle')
						.style('color', 'black')
						.text(function (d) {
							return d.sleepTime
						});

					if (sleepGoal) {
						svg.append('text')
							.attr('transform', translate(x(d3.max(data, getDate)), y(sleepGoal)))
							.attr('x', 38)
							.text('sleep goal');
					}
				}
			}

			scope.$watch('[data, sleepGoal]', initChart, true);

			$(window)
				.resize(function (e) {
					if (element.is(':visible')) {
						initChart();
					}
				});
		}

		return {
			restrict: 'E',
			template: '<div class="chart"></div>',
			scope: {
				data: '=',
				sleepGoal: '='
			},
			link: link
		}
	})
	.directive('sleepGrid', function ($timeout, timeService) {
		var data, width, height;

		var minHeight = 50;
		var getSize = function () {
			width = $(window)
				.width() / 1.05;
			var desiredHeight = $(window)
				.height() / 14;
			height = desiredHeight > minHeight ? desiredHeight : minHeight;
		};

		var translate = function (x, y) {
			return 'translate(' + x + ',' + y + ')';
		}

		var getExtent = function (day) {
			var inputDate = moment(day);
			var begin = inputDate.add(-12, 'hours');
			inputDate = moment(day);
			var end = inputDate.add(12, 'hours');
			return [begin.toDate(), end.toDate()];
		}

		var link = function (scope, element, attrs, ngModelCtrl) {
			var margin = {
				top: 20,
				right: 40,
				bottom: 30,
				left: 100
			};

			function init() {
				$timeout(onLoaded); // init in next cycle, after for loop has finished
			}

			function getSleepTime(entry) {
				var sleepFrom = moment(entry.sleepFrom);
				var sleepTo = moment(entry.sleepTo);
				var sleepSeconds = sleepTo.diff(sleepFrom, 'seconds', true);
				var sleepHours = moment.duration(sleepSeconds, 'seconds')
					.asHours();
				return timeService.formatHours(sleepHours);
			}

			function onLoaded() {
				if (scope.data) {
					data = scope.data;
					var formatTime = scope.localization.timeFormat === "AM/PM"
						? d3.time.format('%I:%M') : d3.time.format('%H:%M');

					d3.selectAll('.sleep-grid svg')
						.remove();

					getSize();
					width = width - margin.left - margin.right;
					height = height;

					var tooltip = d3.select('.sleep-grid')
						.append('div')
						.attr('class', 'grid-tooltip');

					data.forEach(function (d, i) {
						if (d.entries) {
							var x = d3.time
								.scale()
								.range([0, width])
								.domain(getExtent(d.date));

							var y = d3.scale.linear()
								.range([height, 0])
								.domain([0, 1]);

							var xAxis = d3.svg.axis()
								.scale(x)
								.orient('bottom')
								.ticks(12)
								.innerTickSize(-height)
								.outerTickSize(1)
								.tickPadding(5)
								.tickFormat(function (dt) {
									return moment(dt).hour() === 12 ?
										moment(dt).format('ddd DD') : formatTime(dt);
								});

							var topXAxis = d3.svg.axis()
								.scale(x)
								.orient('top')
								.ticks(0);

							var yAxis = d3.svg.axis()
								.scale(y)
								.orient('left')
								.ticks(0);

							var svg = d3.select('#grid' + i)
								.append('svg')
								.attr('width', width + margin.left + margin.right)
								.attr('height', height + margin.top + margin.bottom)
								.append('g')
								.attr('transform', translate(margin.left, margin.top));

							svg.append('g')
								.attr('class', 'x axis')
								.attr('transform', translate(0, height))
								.call(xAxis);

							svg.append('g')
								.attr('class', 'x axis')
								.call(topXAxis);

							svg.append('g')
								.attr('class', 'y axis')
								.call(yAxis);


							d.entries.forEach(function (e, i) {
								var line = d3.svg.line()
									.x(function (day) {
										return x(new Date(day));
									})
									.y(function (day) {
										return y(0.4); //draw in middle
									});

								svg.append('path')
									.datum([e.sleepFrom, e.sleepTo])
									.attr('class', 'thick-line')
									.attr('d', line)
									.on('mouseover', function () {
										tooltip.html('Sleep time: ' + getSleepTime(e) + '<br/>' + 'Comments: ' + e.comments);
										tooltip.classed('visible', true);
										tooltip.style("top", (d3.event.pageY - 10) + "px");
										tooltip.style("left", (d3.event.pageX + 10) + "px");
									})
									.on('mouseout', function () {
										tooltip.text('');
										tooltip.classed('visible', false);
									});

								svg.append('g')
									.selectAll('text')
									.data([e.sleepFrom, e.sleepTo])
									.enter()
									.append('text')
									.attr('x', function (dt) {
										return x(new Date(dt));
									})
									.attr('y', function (dt) {
										return y(0.6);
									})
									.attr('transform', function (dt, ind) {
										return translate(ind === 0 ? 15 : -15, 0);
									})
									.style('text-anchor', 'middle')
									.style('font-weight', 'bold')
									.style('color', 'black')
									.text(function (dt) {
										return scope.localization.timeFormat === "AM/PM"
											? moment(dt).format('hh:mm A') : moment(dt).format('HH:mm');
									});
							});
						}
					});
				}
			}

			scope.$watch('data', init, false);

			$(window)
				.resize(function (e) {
					init();
				});
		};
		return {
			restrict: 'E',
			templateUrl: '/templates/sleepGrid',
			scope: {
				data: '=',
				localization: '='
			},
			link: link
		}
	})
	.directive('dateTimePicker', function (timeService) {
		return {
			restrict: 'E',
			templateUrl: '/templates/dateTimePicker',
			scope: {
				fromDate: '=',
				toDate: '=',
				fromTime: '=',
				toTime: '=',
				sleepTime: '=',
				timeFormat: '=',
				dateFormat: '='
			},
			link: function (scope, element, attrs, ngModelCtrl) {
				var datePicker = $('#date-picker');
				var fromClockInput = $('#from-clock-input');
				var toClockInput = $('#to-clock-input');

				datePicker.datetimepicker({
					useCurrent: false,
					allowInputToggle: true,
					format: 'dddd ' + scope.dateFormat,
					date: moment()
						.startOf('day')
						.add(-2, 'hours'),
					ignoreReadonly: true,
					maxDate: moment()
						.startOf('day')
						.add(23, 'hours')
				});

				datePicker.on('dp.change', function (e) {
					calculate();
				});

				var calculate = function () {
					var fromDate = datePicker.data('DateTimePicker')
						.date();
					var fromSplitted = fromClockInput.prop('value')
						.split(':');
					var toSplitted = toClockInput.prop('value')
						.split(':');
					var fromTime = new Time(Number(fromSplitted[0]), Number(fromSplitted[1]));
					var toTime = new Time(Number(toSplitted[0]), Number(toSplitted[1]));
					var toDate = moment(fromDate);
					if (toTime.compare(fromTime) < 0) {
						toDate.add(24, 'hours');
					}
					fromDate.add(fromTime.hours, 'hours');
					fromDate.add(fromTime.minutes, 'minutes');
					toDate.add(toTime.hours, 'hours');
					toDate.add(toTime.minutes, 'minutes');
					scope.$apply(function () {
						scope.fromDate = fromDate;
						scope.toDate = toDate;
						scope.sleepTime = toDate.to(fromDate, true);
					});
				}

				var fromClock = $('#from-clock');
				var toClock = $('#to-clock');
				fromClock.clockpicker({
					donetext: 'OK',
					autoclose: true,
					twelvehour: false,
					changed: function (time) {
						calculate();
					}
				});
				toClock.clockpicker({
					donetext: 'OK',
					autoclose: true,
					changed: function (time) {
						calculate();
					}
				});

				scope.$watch('fromTime', function (time) {
					if (time) {
						fromClockInput.prop('value', time.toString());
					}
				});
				scope.$watch('toTime', function (time) {
					if (time) {
						toClockInput.prop('value', time.toString());
					}
				});
			}
		};
	})
	.directive('dateSelector', function () {
		return {
			restrict: 'E',
			template: "<div id='date-selector'><input style='display:none'/> <button class='btn btn-default round-btn' ng-click='toggleSelector()'><i class='fa fa-calendar'/></button></div>",
			scope: {
				date: '='
			},
			link: function (scope, element, attrs, ngModelCtrl) {
				var dateSelector = $('#date-selector');
				dateSelector.datetimepicker({
					useCurrent: false,
					format: 'dddd DD.MM.YYYY',
					maxDate: new Date(),
					date: new Date()
				});

				dateSelector.on('dp.change', function (e) {
					dateSelector.data('DateTimePicker')
						.hide();
					if (e.date) {
						dateSelector.data('DateTimePicker')
							.date(null);
						scope.$apply(function () {
							scope.$parent.currentDate = e.date.toDate();
							scope.$parent.loadData();
						});
					}
				});

				scope.toggleSelector = function () {
					dateSelector.data('DateTimePicker')
						.toggle();
				};
			}
		}
	})
	.directive('optionsDialog', function () {
		var onClick = function (dialog, btn) {
			if (dialog.hasClass('show')) {
				dialog.removeClass('show');
				btn.removeClass('btn-border');
				btn.addClass('btn-no-border');
			} else {
				dialog.addClass('show');
				btn.removeClass('btn-no-border');
				btn.addClass('btn-border');
			}
		}

		return {
			restrict: 'E',
			templateUrl: 'templates/optionsDialog',
			scope: {
				day: '=',
				entry: '='
			},
			link: function (scope, element, attrs) {
				var btn = element.find('.dialog-toggle');
				var dialog = element.find('.options-dialog');
				btn.bind('click', function () {
					onClick(dialog, btn);
				});
				scope.$watch('day.show', function (show) {
					if (!show && dialog.hasClass('show')) {
						dialog.removeClass('show');
						btn.addClass('btn-no-border');
					}
				});
			}
		}
	})
	.directive('userSettings', function () {
		var onClick = function (userSettings) {
			if (userSettings.hasClass('show')) {
				userSettings.removeClass('show');
			} else {
				userSettings.addClass('show');
			}
		}

		return {
			restrict: 'E',
			templateUrl: 'templates/userSettings',
			scope: true,
			link: function (scope, element, attrs) {
				var btn = element.find('.menu-toggle');
				var userSettings = element.find('.user-settings');
				btn.bind('click', function () {
					onClick(userSettings);
				})
			}
		}
	})
	.directive('userStats', function () {
		var onClick = function (userStats) {
			if (userStats.hasClass('show')) {
				userStats.removeClass('show');
			} else {
				userStats.addClass('show');
			}
		}

		return {
			restrict: 'E',
			templateUrl: 'templates/userStats',
			scope: true,
			link: function (scope, element, attrs) {
				var btn = element.find('.stats-toggle');
				var userStats = element.find('.user-stats');
				btn.bind('click', function () {
					onClick(userStats);
				})
			}
		}
	})
	.directive('menuItem', function () {
		var onClick = function (content) {
			if (content.hasClass('show')) {
				content.removeClass('show');
			} else {
				content.addClass('show');
			}
		}

		return {
			restrict: 'E',
			replace: true,
			scope: false,
			templateUrl: function (element, attrs) {
				return 'templates/' + attrs.template
			},
			link: function (scope, element, attrs) {
				var toggleElements = element.find('.menu-item-toggle');
				var contentElement = element.find('.menu-item-content');
				toggleElements.bind('click', function (event) {
					onClick(contentElement);
				});
			}
		}
	});