var mongoose = require('mongoose');

var entrySchema = mongoose.Schema({
	userId: String,
	sleepFrom: Date,
	sleepTo: Date,
	comments: String,
	awakeFeeling: Number
});

module.exports = mongoose.model('Entry', entrySchema);