var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({
	name: String,
	email: String,
	sleepGoal: { type: Number, default: 7 },
	localization: {
		timeFormat: {type: String, default: '24h'},
		dateFormat: { type: String, default: 'DD.MM.YYYY' }
	},
	selectedDateRange: String,
	selectedTemplate: String,
	facebook: {
		id: String,
		token: String,
		email: String,
		name: String
	},
	google: {
		id: String,
		token: String,
		email: String,
		name: String
	}
});

userSchema.methods.generateHash = function (password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function (password) {
	return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', userSchema);