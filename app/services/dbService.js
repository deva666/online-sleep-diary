var Entry = require('../../app/models/entry');
var Feeling = require('../../app/models/feelings');
var User = require('../../app/models/user');

var modelMap = {
	'Entry': Entry,
	'Feeling': Feeling,
	'User': User
}

var hasEntries = function (userId, callback) {
	Entry.count({
		'userId' : userId
	}, function (err, c) {
		callback(err, c);
	});
}

var getFiltered = function (modelName, callback, filter) {
	var model = modelMap[modelName];
	if (typeof model === 'undefined') {
		throw new Error('Unknown model');
	}
	model.find(filter || {}, function (err, docs) {
		if (err) {
			callback(err, null);
		} else {
			callback(null, docs);
		}
	});
}

var updateModel = function (modelName, value, callback, filter) {
	var model = modelMap[modelName];
	if (typeof model === 'undefined') {
		throw new Error('Unknown model')
	}
	model.findOne(filter, function (err, doc) {
		if (err || !doc) {
			callback(err, null);
		} else {
			callback(null, doc);
		}
	});
}

var updateUserSettings = function (user, callback, settings) {
	User.findOne({
		'_id': user._id
	}, function (err, obj) {
		if (err || !obj) {
			callback(err || {
				error: 'Unknown user'
			});
		}
		obj['sleepGoal'] = settings.sleepGoal || obj.sleepGoal;
		obj['localization'] = settings.localization || obj.localization;
		obj['selectedDateRange'] = settings.selectedDateRange || obj.selectedDateRange;
		obj['selectedTemplate'] = settings.selectedTemplate || obj.selectedTemplate;
		obj.save();
		callback(null, obj);
	});
}

var createEntry = function (userId, data, onSuccess, onError) {
	Entry.find({
		$and: [{
			userId: userId
		}, {
			$or: [{
				$and: [{
					sleepFrom: {
						$lt: data.toDate
					}
				}, {
					sleepFrom: {
						$gt: data.fromDate
					}
				}]
			}, {
				$and: [{
					sleepTo: {
						$gt: data.fromDate
					}
				}, {
					sleepTo: {
						$lt: data.toDate
					}
				}]
			}, {
				$and: [{
					sleepFrom: {
						$lt: data.fromDate
					}
				}, {
					sleepTo: {
						$gt: data.toDate
					}
				}]
			}]
		}]
	}, function (err, docs) {
		if (!err && docs.length === 0) {
			Entry.create({
				userId: userId,
				sleepFrom: data.fromDate,
				sleepTo: data.toDate,
				comments: data.comments,
				awakeFeeling: data.awakeFeeling
			}, function (err, entry) {
				if (err) {
					onError(err);
				} else {
					onSuccess(entry);
				}
			});
		} else {
			onError(err || "This time range conflicts with existing entries");
		}
	});
}

var findEntries = function (filter, onSuccess, onError) {
	Entry.find(filter, function (err, docs) {
		if (err) {
			onError(err);
		} else {
			onSuccess(docs);
		}
	});
}

var deleteEntry = function (filter, onSuccess, onError) {
	Entry.findOne(filter, function (err, doc) {
		if (!err && doc) {
			doc.remove(function (error) {
				if (error) {
					onError(error);
				} else {
					onSuccess();
				}
			});
		}
	});
}

var deleteAllEntries = function (userId, onSuccess, onError) {
	Entry.remove({ userId: userId }, function (err) {
		if (err) {
			onError(err);
		} else {
			onSuccess();
		}
	});
}


module.exports = {
	createEntry: createEntry,
	deleteEntry: deleteEntry,
	deleteAllEntries: deleteAllEntries,
	findEntries: findEntries,
	getFiltered: getFiltered,
	updateUserSettings: updateUserSettings,
	hasEntries: hasEntries
}