var User = require('../models/user');

var updateSettings = function (user, settings) {
	User.findOne({
		'_id': user._id
	}, function (err, obj) {
		if (err || !obj) {
			return;
		}
		Object.keys(settings)
			.forEach(function (k) {
				obj[k] = settings[k];
			});
		obj.save();
	})
};

module.exports = function () {
	updateSettings: updateSettings
}