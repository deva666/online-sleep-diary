var moment = require('moment');

/**
Returns an array of all days in given week
*/
var getWeekDays = function (date) {
	var weekStart = moment(date)
		.startOf('isoWeek');
	var tomorrow = moment()
		.startOf('day')
		.add(1, 'days');
	var days = [];
	for (var i = 0; i < 7; i++) {
		var day = moment(weekStart);
		if (day.isBefore(tomorrow)) {
			days.push(createDateObject(day));
			weekStart.add(1, 'days');
		} else {
			return days;
		}
	}
	return days;
}

/**
Returns an array of all days in given month
*/
var getMonthDays = function (date) {
	var monthStart = moment(date)
		.startOf('month');
	var monthEnd = moment(monthStart)
		.endOf('month');
	var dayCount = monthEnd.diff(monthStart, 'days');
	var tomorrow = moment()
		.startOf('day')
		.add(1, 'days');
	var days = [];
	for (var i = 0; i <= dayCount; i++) {
		var day = moment(monthStart);
		if (day.isBefore(tomorrow)) {
			days.push(createDateObject(day));
			monthStart.add(1, 'days');
		} else {
			return days;
		}
	}
	return days;
}

var createDateObject = function (day) {
	return {
		dayShortName: day.format('ddd'),
		dayName: day.format('dddd'),
		dayOfMonth: day.format('Do'),
		shortDate: day.format('DD/MM'),
		position: day.date(),
		dayOfYear: day.format('DDDD'),
		date: day.toDate(),
		momentDate: day
	}
}

/**
Formats string represantation of decimal hours into hours and minutes.
eg. input 8.5 returns 08:30
*/
var formatHours = function (input) {
	var hours = typeof input === 'String' ? input : String(input);
	var splitted = hours.split('.');
	var formatted = '';
	if (splitted[0].length === 1) {
		formatted += '0' + splitted[0];
	} else {
		formatted += splitted[0];
	}
	if (typeof splitted[1] === 'undefined') {
		formatted += ':00';
	} else {
		var minutesPart = '0.' + splitted[1];
		var minutes = Math.round((Number(minutesPart) * 60));
		formatted += ':' + (minutes < 10 ? '0' + minutes : minutes);
	}
	return formatted;
}

module.exports = {
	getWeekDays: getWeekDays,
	getMonthDays: getMonthDays,
	formatHours: formatHours
}