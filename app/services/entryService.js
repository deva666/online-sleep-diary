var moment = require('moment');
var feelings = require('../../app/models/feelings');
var dateService = require('../../app/services/dateService');


var getFeelingRate = function (feeling) {
	for (var index = 0; index < feelings.length; index++) {
		var value = feelings[index];
		if (value.text == feeling) {
			return value.rate;
		}
	}
	return 0;
}

var getMaxFeelingRate = function () {
	return Math.max.apply(null, feelings.map(function (f) {
		return f.rate
	}));
}

/**
Groups Sleep entries by Date
Returns an object with dates as keys and Sleep entries as values
*/
var groupEntries = function (docs) {
	var groups = {};
	docs.forEach(function (d) {
		var sleepFrom = moment(d.sleepFrom);
		var sleepTo = moment(d.sleepTo);
		var sleepTime = sleepTo.diff(sleepFrom, 'seconds', true);
		var borderTime = moment(sleepFrom)
			.startOf('day')
			.add(12, 'hours');
		var group = '';
		var groupDate = null;
		if (sleepFrom.isAfter(borderTime)) {
			groupDate = moment(sleepFrom)
				.add(1, 'days');
		} else {
			groupDate = borderTime;
		}
		group = groupDate.format('DDDD');
		groups[group] = groups[group] || {
			entries: [],
			sleepTime: 0,
			awakeFeeling: ''
		};
		groups[group].entries.push(d);
		groups[group].sleepTime += sleepTime;
		groups[group].awakeFeeling = d.awakeFeeling;
	});
	return groups;
}

var getOverallStats = function (entries) {
	var hasEntries = entries ? entries.length > 0 : false;
	var grouped = groupEntries(entries);
	var totalSleepSecs = 0;
	var daysCount = 0;
	var maxSleepTime = 0;
	var minSleepTime = Number.MAX_VALUE;
	var restedSleepSecs = 0;
	var restedCount = 0;
	var restedFeelingRate = getMaxFeelingRate();

	Object.keys(grouped)
		.forEach(function (k) {
			daysCount++;
			totalSleepSecs += grouped[k].sleepTime;
			maxSleepTime = grouped[k].sleepTime > maxSleepTime ? grouped[k].sleepTime : maxSleepTime;
			minSleepTime = grouped[k].sleepTime < minSleepTime ? grouped[k].sleepTime : minSleepTime;

			if (grouped[k].awakeFeeling === restedFeelingRate) {
				restedCount++;
				restedSleepSecs += grouped[k].sleepTime;
			}
		});

	return {
		hasEntries: hasEntries,
		averageSleepTime: dateService.formatHours(moment.duration(totalSleepSecs / daysCount,
			'seconds')
			.asHours()),
		maxSleepTime: dateService.formatHours(moment.duration(maxSleepTime,
			'seconds')
			.asHours()),
		minSleepTime: dateService.formatHours(moment.duration(minSleepTime === Number.MAX_VALUE ? 0 : minSleepTime,
			'seconds')
			.asHours()),
		averageRestedTime: dateService.formatHours(moment.duration(restedSleepSecs / restedCount,
			'seconds')
			.asHours()),
		sleepEfficiency: Math.round(100 * restedCount / daysCount)
	};
}

var getDateRangeData = function (docs, requestedDate, dateRange) {
	var grouped = groupEntries(docs);
	var days = dateRange === 'isoWeek' ? dateService.getWeekDays(requestedDate)
		: dateService.getMonthDays(requestedDate);
	var totalSleepSecs = 0;
	var entryCount = 0;
	var maxSleepTime = 0;
	var minSleepTime = Number.MAX_VALUE;
	var restedCount = 0;
	var restedFeelingRate = getMaxFeelingRate();
	var isDataEmpty = docs.length === 0;

	days.forEach(function (d) {
		var group = grouped[d.dayOfYear];
		if (typeof group !== 'undefined') {
			d['entries'] = group.entries;
			d['awakeFeeling'] = group.awakeFeeling;
			d['show'] = false;
			var hours = moment.duration(group.sleepTime, 'seconds')
				.asHours();
			d['sleepTime'] = dateService.formatHours(hours);
			d['sleepHours'] = hours;
			totalSleepSecs += group.sleepTime;
			entryCount++;
			if (group.awakeFeeling === restedFeelingRate) {
				restedCount++;
			}
			maxSleepTime = group.sleepTime > maxSleepTime ? group.sleepTime : maxSleepTime;
			minSleepTime = group.sleepTime < minSleepTime ? group.sleepTime : minSleepTime;
		}
	});

	var data = {
		isDataEmpty: isDataEmpty,
		days: days,
		stats: {
			averageSleepTime: dateService.formatHours(moment.duration(totalSleepSecs / entryCount,
				'seconds')
				.asHours()),
			maxSleepTime: dateService.formatHours(moment.duration(maxSleepTime, 'seconds')
				.asHours()),
			minSleepTime: dateService.formatHours(moment.duration(minSleepTime == Number.MAX_VALUE ? 0 :
				minSleepTime,
				'seconds')
				.asHours()),
			sleepEfficiency: Math.round(100 * restedCount / entryCount)
		}
	}

	return data;
}


module.exports = {
	getDateRangeData: getDateRangeData,
	getOverallStats: getOverallStats
}