var moment = require('moment');
var async = require('async');
var Entry = require('../app/models/entry');
var awakeFeelings = require('../app/models/feelings');
var entryService = require('../app/services/entryService');
var userService = require('../app/services/userService');
var dbService = require('../app/services/dbService');
var buildConfig = require('../config/buildConfig');

function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}

	res.redirect('/');
}

module.exports = function (app, passport) {
	app.get('/', function (req, res) {
		if (req.user) {
			res.redirect('/user');
		} else {
			res.render('index', {
				title: app.appName,
				path: req.path,
				isHomePage: true,
				isDebugging: buildConfig.isDebugging
			});
		}
	});

	app.get('/user', isLoggedIn, function (req, res) {
		dbService.hasEntries(req.user._id, function (err, count) {
			res.render('user', {
				user: {
					sleepGoal: req.user.sleepGoal,
					name: req.user.name,
					localization: req.user.localization,
					selectedTemplate: req.user.selectedTemplate,
					selectedDateRange: req.user.selectedDateRange,
					isHomePage: false,
					hasEntries: !err && count > 0
				},
				title: req.user.name ? req.user.name + ' Sleep diary' : 'Sleep diary',
				path: req.path,
				isDebugging: buildConfig.isDebugging
			});
		});
	});

	app.get('/user/data', isLoggedIn, function (req, res) {
		var requestedDate = req.query.date;
		var dateRange = req.query.dateRange;
		var firstDate = moment(requestedDate)
			.startOf(dateRange);
		var endDate = moment(requestedDate)
			.endOf(dateRange);
		var filterDateRange = {
			$and: [{
				userId: req.user._id
			}, {
					sleepTo: {
						$gte: firstDate.toDate()
					}
				}, {
					sleepTo: {
						$lte: endDate.toDate()
					}
				}]
		};
		var filterAll = {
			userId: req.user._id
		};
		async.parallel([function (callback) {
			dbService.getFiltered('Entry', callback, filterDateRange);
		}, function (callback) {
			dbService.getFiltered('Entry', callback, filterAll);
		}], function (err, results) {
			if (err) {
				res.status(500)
					.json({
						error: err
					});
			} else {
				var data = {
					dateRange: entryService.getDateRangeData(results[0], requestedDate, dateRange),
					overallStats: entryService.getOverallStats(results[1]),
					hasEntries: results[1].length > 0,
					sleepGoal: req.user.sleepGoal,
					today: moment()
						.startOf('day')
				}
				res.json(data);
			}
		});
	});

	app.get('/feelings', function (req, res) {
		res.json(awakeFeelings);
	});

	app.post('/user/data', isLoggedIn, function (req, res) {
		var data = req.body;
		dbService.createEntry(req.user._id, data, function (entry) {
			res.json(entry);
		}, function (err) {
			res.status(500)
				.json({
					error: err
				});
		});
	});

	app.delete('/user/data/:id', isLoggedIn, function (req, res) {
		var entryId = req.params.id;
		if (entryId) {
			var filter = {
				_id: entryId
			};

			dbService.deleteEntry(filter, function (err) {
				res.status(200)
					.json({
						'status': 'OK'
					});
			}, function () {
				res.status(500)
					.json({
						error: err
					});
			});
		}
	});

	app.post('/user/settings', isLoggedIn, function (req, res) {
		var settings = req.body;
		dbService.updateUserSettings(req.user, function (err, obj) {
			if (err) {
				res.status(500)
					.json({
						error: err
					});
			} else {
				res.json(obj);
			}
		}, settings);
	});

	app.get('/logout', function (req, res) {
		req.logout();
		res.redirect('/');
	});

	app.get('/partials/:name', function (req, res) {
		var name = req.params.name;
		res.render('partials/' + name);
	});


	app.get('/templates/:name', function (req, res) {
		var name = req.params.name;
		res.render('templates/' + name);
	});

	app.get('/auth/facebook', passport.authenticate('facebook', {
		scope: 'email'
	}));

	app.get('/auth/facebook/callback',
		passport.authenticate('facebook', {
			successRedirect: '/user',
			failureRedirect: '/'
		}));

	app.get('/auth/google', passport.authenticate('google', {
		scope: ['profile', 'email']
	}));

	app.get('/auth/google/callback',
		passport.authenticate('google', {
			successRedirect: '/user',
			failureRedirect: '/'
		}));

	app.get('/connect/facebook', passport.authorize('facebook', {
		scope: 'email'
	}));

	app.get('/connect/facebook/callback',
		passport.authorize('facebook', {
			successRedirect: '/user',
			failureRedirect: '/'
		}));

	app.get('/connect/google', passport.authorize('google', {
		scope: ['user', 'email']
	}));

	// the callback after google has authorized the user
	app.get('/connect/google/callback',
		passport.authorize('google', {
			successRedirect: '/user',
			failureRedirect: '/'
		}));


	// facebook -------------------------------
	app.get('/unlink/facebook', isLoggedIn, function (req, res) {
		var user = req.user;
		dbService.deleteAllEntries(user._id, function () {
			user.facebook.token = undefined;
			user.save(function (err) {
				res.redirect('/');
			});
		}, function (err) {
			res.status(500);
		});
	});

	// google ---------------------------------
	app.get('/unlink/google', isLoggedIn, function (req, res) {
		var user = req.user;
		dbService.deleteAllEntries(user._id, function () {
			user.google.token = undefined;
			user.save(function (err) {
				res.redirect('/');
			});
		}, function (err) {
			res.status(500);
		}
		)
	});

	app.get('/unlink', isLoggedIn, function (req, res) {
		var user = req.user;
		var isGoogle = user.google !== 'undefined' || user.google.token !== 'undefined';
		dbService.deleteAllEntries(user._id, function () {
			if (isGoogle) {
				user.google.token = undefined;
			} else {
				user.facebook.token = undefined;
			}
			user.save(function (err) {
				res.redirect('/');
			});
		}, function (err) {
			res.status(500);
		}
		)
	});
};
